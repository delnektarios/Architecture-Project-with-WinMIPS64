;-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
	.data

;array:  .word 237,514,504,275,409,766
;	.word 902,91,943,273,828,160,103,106,713,100,887,147,584,306,565,163,312,573,891,100,363,342,81,982,851,432,202,307,5,420
;	.word 238,181,5,808,499,998,578,709,886,28,826,108,875,494,280,173,464,33,100,100,647,838,336,76,469,968,82,627,907,828
;	.word 173,691,790,252,471,453,914,10,667,942,800,740,770,549,637,250,434,209,586,202,903,487,241,635,299,200,380,948,116,712

;array: .word 100,4,16,33,5,9,1,3,7,3,9,27,67,59,234,567,48,28,78,43,25,739,467,965,156,753,356,45,166,326,3,656,45,67,17,356,356,24,145,256,56,256,25,76,156,886,678,68,1,48,336,67
;	.word 13,1,16,61,651,16,13,1,71,61,17,81,18,651,1,61,18,516,1,761,15,1,1,1,136,1,1,61,12,176,1,177,16,1,27,3,4,5,6,7,8,9,4,6,1,78

array:  .word 10,4,16,3,5,9,1,3,7,3,9,27,67,59,234,7,48,28,78,43,25,7,46,96,156,7,3,45,16,32,30,65,45,6,16,350,356,24,145,256,56,256,25,76,15,86,68,68,10,45,35,60
	.word 13,10,16,61,65,16,13,10,7,6,7,8,18,65,10,60,18,56,15,760,15,15,10,100,136,18,18,60,12,176,1,177,16,1,27,3,4,5,6,7,8,9,4,6,19,78
	       
ERROR: .asciiz "ERROR number of limits" ;error message
		   
CONTROL: .word32 0x10000
DATA:    .word32 0x10008
;-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
        .text
lwu r21,CONTROL($zero)
lwu r22,DATA($zero)
                                                             
daddi r10,r10,99  ;number of integers in array

for:
	
;------------------------------------------Check if 0<r1<1001----------------------------
	ld r1,array(r14)        	 ;Loading number
	dadd r15,r14,$zero		 ;Using r15 to keep the first address of the three numbers in order to paste after the GCM on them 
	slti r6,r1,1001   ;1st       
	slt r30,$zero,r1  ;2nd
	daddi r14,r14,8			 ;Using the increament of R14 that helps me get the next element of the array . Also preventing stall
	bne r30,r6,exit         	 ; Only if the 1st and the 2nd slt have different outcomes the loaded value is between 0 and 1001
	
;------------------------------------------------------------------------------------------
	

;----------------------------------------------Calculating GCM between 1st and 2nd and 3rd number------------ --------------------------------------------	

;------------------check if 0<r2<1001----------------------------
	ld r2,array(r14)                  ;Loading 2nd number 
	daddi r14,r14,8                   ;Using the increament of R14 that helps us get the next element of the array . Also preventing stall
	slti r6,r2,1001	  ;1st
	slt r30,$zero,r2  ;2nd
	slt r8,r2,r1                      ; Using to see if (r2<r1) for the next bnez
	bne r30,r6,exit                   ;  Only if the 1st and the 2nd slt have different outcomes the loaded value is between 0 and 1001
;------------------------------------------------------------------
	bnez r8,labelA    ; If r2<=r1 then goto(labelA) else swap numbers to avoid the substracting value being bigger than the substracted value
		xor r2,r2,r1
		xor r1,r1,r2
		xor r2,r2,r1
    labelA:
;----------------------------------------------Calculating GCM of the first and second number  (Euclid Algo)-----------------------------------------------------
		dsub r1,r1,r2     ; I decrease the r1 the first time out of the whileB to swap the possition of the command slt and dsub to prevent raw stall 
		slt r6,r1,r2	  ; Checking if r1<r2 and jump to branch
		j BRANCH
	whileA:
		whileB:                           ;calculate module between r1%r2
			slt r6,r1,r2
			dsub r1,r1,r2
	BRANCH:		
		beqz r6,whileB     	; If (r1=>r2) branch goto wlileB
		dadd r1,r1,r2      	; Because of the switch of slt and dsub the program does one more unnecessary decrease of the r1 so add it back once     
		xor r2,r2,r1	   	;Swaping elements
		xor r1,r1,r2
		xor r2,r2,r1      	;r1 holds gcf
	bnez r2,whileA 	
;----------------------------------------------------------------------------------------------------------------------------------------------------------	
;----------------------------------------------Calculating GCM between the GCM of the first two nubers and the 3rd-----------------------------------------		

;------------------Checking 0<r2<1001----------------------------
	ld r2,array(r14)		;Loading 2nd number
	daddi r14,r14,8         	;Using the increase of R14 that helps me get the next element of the array . Also preventing stall          
	slti r6,r2,1001	  ;1st
	slt r30,$zero,r2  ;2nd
	daddi r10,r10,-3        	;Decrease of the loop integer.Also prevents stall
	slt r8,r2,r1            	;Comparing if the GCM of the first two numbers is bigger than the third
	bne r30,r6,exit          	;Only if the 1st and the 2nd slt have different outcomes the loaded value is between 0 and 1001
;------------------------------------------------------------------
	bnez r8,label1    		; if(r2<r1) then goto lable1(label1) else swap elements.
		xor r2,r2,r1
		xor r1,r1,r2
		xor r2,r2,r1
                     
	label1:
		dsub r1,r1,r2 	;I decrease the r1 the first time out of the whileB so i can swap the position of the command slt and dsub to prevent raw stall  
		slt r6,r1,r2  		;Checking if r1<r2 and jump to branch
		j BRANCH1     
		                                                                            
	while1:
		while2:      		;calculate module between the GCM of the firs to numbers and the 3rd
			slt r6,r1,r2
			dsub r1,r1,r2
	BRANCH1:		
		beqz r6,while2  	;If (r1=>r2) branch goto while2
		dadd r1,r1,r2		;Because of the switch of slt and dsub the program does one more unnecessary decrease of the r1 so I add it back  
		xor r2,r2,r1
		xor r1,r1,r2
		xor r2,r2,r1    	;r1 holds gcf
	bnez r2,while1
;--------------------------------------------------------------------------------------------------------------------------------------------														  
	sd r1,array(r15)       		; Inserting GCD in the array at the appropriate memory cells                                                                          
	daddi r15,r15,8
	sd r1,array(r15)
	daddi r15,r15,8
	sd r1,array(r15)
	daddi r15,r15,8	  	
	bnez r10,for               		; Big loop for 100 integers
	halt															
exit:
	daddi r24,$zero,4      ; Printing ERROR if something goes wrong
	daddi r23,$zero,ERROR
	sd r23,(r22)
	sd r24 ,(r21)
	halt
	